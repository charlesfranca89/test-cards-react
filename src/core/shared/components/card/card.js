import React from 'react';

export default function Card(props) {
    return (
        <div 
            className={"card" + (props.card.active ? ' active' : '')}
            onClick={() => props.itemClicked(props.card)}>
            <img
                className="close" 
                src={ require("../../../../assets/images/icons/close-icon.png") } alt="" 
                onClick={(e) => {
                    props.closeClicked(props.card);
                    e.stopPropagation();
                }}/>
            <div className="description">{props.card.description}</div>
            <div className="percent">{props.card.percent}</div>
        </div>
    )
}