import React, { useState, useEffect} from 'react';
import Card from './card';

export default function CardList() {
    const [cards, setCads] = useState([
        {
            description: 'Percent Accuracy',
            percent: '87.0%',
            active: false
        },
        {
            description: 'Percent Accuracy',
            percent: '34.0%',
            active: false
        },
        {
            description: 'Percent Accuracy',
            percent: '63.0%',
            active: false
        },
        {
            description: 'Percent Accuracy',
            percent: '82.0%',
            active: false
        },
        {
            description: 'Percent Accuracy',
            percent: '32.0%',
            active: false
        },
        {
            description: 'Percent Accuracy',
            percent: '42.0%',
            active: false
        }
    ]);
    const [listType, setListType] = useState('list');
    const [activeIndex, setActiveIndex] = useState(null);
    return (
        <div className={"cards " + listType}>
          {
              cards.map((card, index) => {
                  return (
                      <Card 
                        key={index.toString()}
                        card={card}
                        itemClicked={(item) => {
                            if (activeIndex >= 0) {
                                setCads(cards.map((c, i) => {
                                    if (i == activeIndex) {
                                        c.active = false;
                                    }
                                    return c;
                                }));
                            }
                            setActiveIndex(index);
                            setCads(cards.map((c, i) => {
                                if (i == index) {
                                    c.active = true;
                                }
                                return c;
                            }));
                            setListType("details");
                        }}
                        closeClicked={(item) => {
                            setCads(cards.map((c, i) => {
                                c.active = false;
                                return c;
                            }));
                            setActiveIndex(null);
                            setListType("list");
                        }} />
                  )
              })
          }
        </div>
    )
}