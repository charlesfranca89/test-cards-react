import React from 'react';
import './App.scss';
import CardList from './core/shared/components/card';

function App() {
  return (
    <div className="main">
      <div className="cards-container">
        <CardList />
      </div>
    </div>
  );
}

export default App;
